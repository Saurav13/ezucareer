@extends('layouts.admin')

@section('body')

    <div class="content-body">
        <section>  
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" ><a data-action="collapse">Results for Banner Ads</a></h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a href="{{ route('bannerAds.index') }}"><i class="icon-arrow-left"></i></a></li>
                                    <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                            
                        <div class="card-body collapse in">                    
                            @if(count($ads) == 0)
                                <h6 style="text-align:  center;margin: 50px;">No expired banner ads.</h6>
                            @else
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th>S.N</th>
                                                <th>Category</th>
                                                <th>Quantity</th>
                                                <th>Image</th>
                                                <th>Expiry Date</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($ads as $ad)
                                                <tr>
                                                    <td>{{ $loop->iteration * $ads->currentPage() }}</td>
                                                    <td>{{ $ad->category->name }}</td>
                                                    <td>{{ $ad->quantity }}</td>
                                                    <td><a href="{{ $ad->link }}" target="_blank" title="{{ $ad->link }}"><img src="{{ asset('bannerad_images/'.$ad->image) }}" width="50px" height="50px"/></a></td>
                                                    <td>{{ date('M j,Y',strtotime($ad->expiry_date)) }}</td>
                                                    <td>
                                                        <a href="{{ route('bannerAds.edit',$ad->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                                        <form action="{{ route('bannerAds.destroy',$ad->id) }}" method="POST" style="display:inline">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="_method" value="DELETE"/>
                                                            <button type="button" id="deleteAd{{$ad->id}}" class="btn btn-sm btn-danger">Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach   
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                            <div class="text-xs-center mb-3">
                                <nav aria-label="Page navigation">
                                    {{ $ads->links() }}
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection